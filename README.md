# How to Run
```
Open index.html in your browser
```
# Notes:
# I. Getting Started
## Installation options
- CSS & JS precomplied
- CDN
- Source files
- Package managers

# II. Basic Styles
## 1. Basic typography
### Typography
- `reboot.css` styles
- `rems` vs `ems`
- Avoid `margin-top`
- `inherit` if possible
- `border-box` sizing
- Native font stacks
- Special styles

### New classes in bootstrap 4
- Heading: `h1`, `h2`, ..., `h6`
- Display: `display-1`, `display-2`,...

## 2. Typographic utilities
### Alignment utilities
- `text-justify`
- `text-nowrap`
- `text(-XX)-POS`
  - XX: `sm`, `md`, `lg`, `xl`
  - POS: `left`, `center`, `right`
  
### Capitalization
- `text-lowercase`
- `text-uppercase`
- `text-capitalize`

### Text styles
- `font-weight-bold`
- `font-weight-normal`
- `font-italic`

## 3. Lists & Blockquotes
### Lists
- `list-unstyled` no bullets
- Inline lists
  - `list-inline` on `ul`
  - `list-inline-item` on `li`
  
### Blockquotes
- `blockquote`, `blockquote-reverse`
- `blockquote-footer`

## 4. Colors
### Colors
- `text-COLOR` for text
  - COLOR: `primary`, `success`, `info`, `warning`, `danger`, `white`
  - Use for links: `primary`, `success`, `info`, `warning`, `danger`, `muted`
- `bg-COLOR` for backgrounds

## 5. Images
### Basic images
- `img-fluid` responsive images
- `rounded`, `rounded-DIR` round edges
  - DIR: `top`, `right`, `bottom`, `left`, `circle`, `0`
- `img-thumbnail` rounded 1px border

### Align images
- `float-left`, `float-right`
- `text-center` center inline
- `mx-auto` center block

### Figures
- `figure` on `<figure>` tag
- `figure-img` on the images
- `figure-caption` on the text

# III. Layouts
## 1. Overviews
### 12-column grid breakpoints
- Extra small
- Small
- Medium
- Large
- Extra large (new in Bootstrap 4)

## 2. Containers and Rows
### The grid
- Responsive 12-column
- Flexbox based
- Structure
  - containers
  - rows/columns

### Grid containers
- `container`
- `container-fluid`
- 15px padding
- Adjust to breakpoints

## 3. Columns
- 12 column grid
- Columns can span
- `col-BP-COL`
  BP: `sm`, `md`, `lg`, `xl`
  COL: 1-12
- Can use multiple columns at different breakpoints
- Offset columns: `offset-BP-COL`
- Nesting columns:
  - `row` inside column
  - Create 12-col grid
  - Use same classes
- Custom orders
  - Flex Order
    - `flex-(-BP)-ORD`
      - BP: `sm`, `md`, `lg`, `xl`
      - ORD: `first`, `last`, `unordered`
  - Push & Pull
    - ACT(-BP)-ORD
      - ACT: `push`, `pull`
      - BP: `sm`, `md`, `lg`, `xl`
      - ORD: 1-12

## 4. Grid alignment options
### Vertical alignment
- Use in rows
- `align-item-ALN`
  - ALN: `start`, `center`, `end`
- Works on nested cols

### Individual alignment
- Use in cols
- `align-self-ALN`
  - ALN: `start`, `center`, `end`

### Horizontal alignment
- Use in rows
- Need col width
- `justify-content-ALN`
  - ALN: `start`, `center`, `end`, `around`, `between`
  
## 5. Display properties
### Position
- Position classes: `fixed-top`, `fixed-bottom`, `sticky-top`

### Display
- Basic display
  - Mimics CSS
  - `d-TYPE`
    - ALN: `block`, `inline`, `inline-block`, `flex (options)`

- Basic flex container
 - `d(-BP)(-inline)-flex`
  - BP: `sm`, `md`, `lg`, `xl`
  
## 6. Flexbox container options
### Flex container
- Container/item classes
- `d(-BP)(-inline)-flex`
  - BP: `sm`, `md`, `lg`, `xl`
### Direction
- `flex(-BP)(-DIR)(-reverse)`
  - BP: `sm`, `md`, `lg`, `xl`
  - DIR: `row`, `column`
  
### Justify
- `justify-content(-BP)-ALG`
  - BP: `sm`, `md`, `lg`, `xl`
  - ALN: `start`, `center`, `end`, `around`, `between`
  
### Wrap
- `flex(-BP)-WBP(-reverse)`
  - BP: `sm`, `md`, `lg`, `xl`
  - WRP: `wrap`, `nowrap`
  
### Vertical alignment
- `align-content(-BP)-ALG`
  - BP: `sm`, `md`, `lg`, `xl`
  - ALN: `start`, `center`, `end`, `around`, `between`, `stretch`

## 7. Element spacing & utilities
### Floating elements
`float-(BP)-SID`
- BP: `sm`, `md`, `lg`, `xl`
- SID: `left`, `right`, `none`
- `clearfix`
  
### Margin/Padding
`PRO(SID)(-BP)-SIZ`
- PRO: `m` - margin, `p` - padding
- SID: `t`, `r`, `b`, `l`, `x`, `y`
- BP: `sm`, `md`, `lg`, `xl`
- SIZ: 0, 1, 2, 3, 4, 5, `auto`

### Sizing
`SIZ(-AMT)`
- SIZ: `w`, `h`, `mw`, `mh`
- AMT: 25, 50, 75, 100

### Visibility
- `invisible` toggles visibility
- `hidden-BP-DIR`
  - BP: `xs`, `sm`, `md`, `lg`, `xl`
  - ALG: `up`, `down`

### Align self
`align-self(-BP)-ALG`
- BP: `sm`, `md`, `lg`, `xl`
- ALG: `start`, `end`, `center`, `baseline`, `stretch`

# IV. Navs & Navbar components
## 1. Overview
### Types
- Navs
- Tabs
- Pills
- Navbars

### Navbar components
- Branding
- Color schemes
- Dropdowns
- Form elements

## 2. Create basic navigation
### Basic nav classes
- With/without `ul`
- `nav`
- `nav-item`
- `nav-link`

### Nav link options
- `active`
- `disabled`

### Nav styles
- `nav-pills`
- `nav-tabs`

### Nav alignment
- `justify-content-center`
- `justify-content-end`
- `nav-fill`
- `nav-justified`
- `flex-column`

## 3. Create a navbar
### Navbar classes
- `navbar`
- `navbar-toggleable-BP`
  - BP: `sm`, `md`, `lg`, `xl`
- `navbar-nav`

### Navbar colors
- `bg-COLOR` for backgrounds
  - COLOR: `primary`, `success`, `info`, `warning`, `danger`, `inverse`, `faded`
- `navbar-light`
- `navbar-inverse`

### `navbar-nav` options
- `nav-item`
- `nav-link`
- `active`
- `disabled`

## 4. Navbar Brand & Text
### Navbar options
- `navbar-brand`
  - Link or headline
  - Using images
- `navbar-text`

## 5. Dropdown on navigation
### Dropdown setup
- `dropdown` to align
- `dropdown-toggle` on link
- `data-toggle="dropdown"`
- `dropdown-menu`
- `dropdown-item`
- `id` and `aria` attributes

## 6. Form elements
### Form classes
- `form-inline`
- `form-control`
- Spacing as needed

## 7. Control placement
### Placement options
- `fixed-top`
- `fixed-bottom`
- `sticky-top`
- `mr-auto`

## 8. Collapsible content
### Collapsible content
- `collapse`
- `navbar-collapse`
- `id`

### Toggler classes
- `navbar-toggler` & alignment
- Other properties
- `navbar-toggler-icon`

# V. Basic Style Elements
## 1. Buttons
### Button options
- `btn`
- `btn-SIZ`
  - SIZ: `sm`, `lg`
- `<button>`, `<a>`, `<input>`

### Button colors
`btn-COLOR`, `btn-outline-COLOR`
- COLOR: `primary`, `secondary`, `success`, `info`, `warning`, `danger`, `link`

### Other options
- `btn-block` full width
- `active`
- `disabled`

## 2. Button groups
### Button group classes
- `btn-group`
- `btn-group-vertical`
- `btn-toolbar`
- `btn-group-SIZ`
  - SIZ: `sm`, `lg`
  
## 3. Badges
### Badge classes
- `badge`
- `badge-pill`
- `badge-COLOR`
  - - COLOR: `primary`, `success`, `info`, `warning`, `danger`, `default`

## 4. Progress bar
### Progress classes
- `progress` containers
- `progress-bar` item
- Style `width`, `height`
- Label text

### Progress styles
- Use `bg-COLOR`
- `progress-bar-striped`
- `progress-bar-animated`
- Multiple bars

### Accessibility properties
- `role="progressbar"`
- `aria-valuenow`
- `aria-valuemin`
- `aria-valuemax`

## 5. List group
### List group classes
- `list-group` containers
- `list-group-item` items
- `li`, `button`, `a`

### List group styles
- `active`, `disabled`
- `list-group-item-action` style
- `list-group-item-COLOR`
- `badge` classes
- `justify-content-between`

## 6. Breadcrumbs
### Breadcrumb links
- `breadcrumb` containers
- `breadcrumb-item` items
- `active` style
- `li`, `nav>a`

# VI. Layout Components
## 1. Jumbotron
### Jumbotron classes
- `jumbotron` container
- `jumbotron-fluid` items
- Use styles as needed

## 2. Table
### Table classes
- `table`
- `table-inverse`
- `thead-inverse`
- `table-striped`
- `table-bordered`
- `table-hover`
- `table-sm`
- `table-responsive`

### Table colors
- Use `table-COLOR` on `row`, `td`

## 3. Card
### Card class
- `card` container

### Card styling
- `card-COLOR`
- `card-inverse`
- `card-outline-COLOR`

### Card items
- `card-block`
- `card-text`
- `card-title`
- `card-subtitle`
- `card-link`

### Card images
- `card-img-top`
- `card-img-bottom`
- `card-img-overlay`

## 4. Card content
### Headers & Footers
- `card-header`
- `card-footer`

### List & Tabs
- `list-group-flush`
- `card-header-tabs`

## 5. Card layouts
### Traditional layouts
- `col` grid
- Flexbox grid

### Layout classes
- `card-group` containers
- `card-deck`
- `card-columns`

## 6. Media element
### Media classes
- `media` containers
- `media-body`
- Use flexbox classes

# VII. Forms
## 1. Basic form
### Form classes
- `form-group`
- `form-text`

### Form controls
- `form-control`
- `form-control-label`
- `form-control-file`

## 2. Checkboxes & radio buttons
### Form check classes
- `form-check`
- `form-check-label`
- `form-check-input`
- `form-check-inline`

## 3. Size & validation styles
### Form styles
- `form-control-sm`
- `form-control-lg`
- `form-inline`

### Validation styles
- `has-COLOR`
- `form-control-COLOR`

## 4. Multicolumn forms
### Form styles
- Needs `container`
- Use `row`, `col`
- `col-form-label`

## 5. Input groups
### Input group
- `input-group-addon`
- Add on each side
- Other elements

# VIII. Interactive Components
## 1. Tooltips
### Using tooltips
- Links or Controls
- Use `tether` library
- `data-toggle="tooltip"`
- `title="text"`

### Activating
- JavaScript or `data`

### Common options
- `placement`: `top`, `right`, `bottom`, `left`
- `trigger`: `click`, `hover`, `focus`
- `html`: `true`, `false`

## 2. Popovers
- Using `tether` library
- `data-toggle="popover"`
- `title="text"`
- `data-content="content"`
- `placement`: `top`, `right`, `bottom`, `left`
- `trigger`: `click`, `hover`, `focus`
- `container`

## 3. Alerts
### Setting up alerts
- `alert`
- `alert-COLOR`

### Alert content
- `alert-heading`
- `alert-link`

### Dismissable alerts
- `alert-dismissable`, `fade`, `show`
- Add a close button

## 4. Dropdowns
### Dropdown basics
- Navs, Tabs, etc.
- Button/Nav Trigger
- links or buttons

### Dropdown classes
- `dropdown`
- `dropdown`
- `dropdown-item`
- `dropdown-toggle`

### Dropdown elements
- `dropdown-header`
- `dropdown-divider`
- `disabled`

### Dropdown options
- `btn-sm`, `btn-lg`
- `dropup`
- `dropdown-menu-right`
- `btn-group`, `dropdown-toggle-split`

## 5. Collapse & accordions
### Collapse
- Link or Button
- `data-toggle="collapse"`
- `id` or `data-target`
- `collapse` class

### Accordion
- Require container
- `show` class once
- `dropdown-menu`
- Use `card`

## 6. Modal
### Modal
- Button or Link
- `id` or `data-target`
- `data-toggle="modal`
- `modal`

### Structural classes
- `modal-dialog`
- `modal-content`
- `modal-header`
- `modal-body`
- `modal-footer`

### Modal options
- `modal-title`
- `data-dismiss="modal"`

## 7. Carousel
### Setup
- `carousel`
- `data-ride="carousel"`
- `carousel-inner`
- `carousel-item`

### Options
- One element `active`
- Crop and size photos

### Captions
- `carousel-caption`

### Navigation
- `data-target`
- `carousel-control-prev`
- `carousel-control-prev-icon`
- `carousel-control-next`
- `carousel-control-next-icon`

### Indicators
- `carousel-indicators`
- `data-target`
- `data-slide-to`

### Setup
- Javascript or `data-`

### Data attributes
- `interval`: 5000
- `pause`: hover | null
- `ride`: false
- `wrap`: true

## 8. Scrollspy
### Using scrollspy
- `data-spy="scroll"`
- `position: relative`
- `data-target="ID"`
- `fixed-top`
- `data-offet`

